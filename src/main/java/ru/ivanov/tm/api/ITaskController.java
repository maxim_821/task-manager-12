package ru.ivanov.tm.api;

public interface ITaskController {

    void showList();

    void create();

    void clear();

    void showTaskByIndex();

    void showTaskByName();

    void showTaskById();

    void removeTaskByIndex();

    void removeTaskByName();

    void updateTaskByIndex();

    void updateTaskById();

    void startTaskById();

    void startTaskByIndex();

    void startTaskByName();

    void finishTaskById();

    void finishTaskByIndex();

    void finishTaskByName();

    // void changeProjectStatusById();

    // void changeProjectStatusByName();

    //  void changeProjectStatusByIndex();

}
